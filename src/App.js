import { Route, Routes } from "react-router-dom";
import Home from "./pages/Home";
import IphoneXDetail from "./pages/IphoneXDetail";
import NokiaDetail from "./pages/NokiaDetail";
import SamsungDetail from "./pages/SamsungDetail";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home/>}/>
      <Route path="/iphonex" element={<IphoneXDetail/>}/>
      <Route path="/samsung" element={<SamsungDetail/>}/>
      <Route path="/nokia" element={<NokiaDetail/>}/>
    </Routes>
  );
}

export default App;
