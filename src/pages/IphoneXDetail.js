import { Card, CardContent, Container, CardActions, Button, Typography } from "@mui/material"
import LabelTitle from "../components/labelTitle"

const IphoneXDetail = () => {
    return (
        <Container>
            <LabelTitle/>
            <Card>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Iphone X
                    </Typography>
                    <Typography>Price: 900$</Typography>
                    <Typography>Quantity: 0</Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" variant="contained" href="/">Back</Button>
                </CardActions>
            </Card>
        </Container>
    )
}

export default IphoneXDetail