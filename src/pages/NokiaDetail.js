import { Card, CardContent, Container, CardActions, Button, Typography } from "@mui/material"
import LabelTitle from "../components/labelTitle"

const NokiaDetail = () => {
    return (
        <Container>
            <LabelTitle/>
            <Card>
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        Nokia 8
                    </Typography>
                    <Typography>Price: 650$</Typography>
                    <Typography>Quantity: 0</Typography>
                </CardContent>
                <CardActions>
                    <Button size="small" variant="contained" href="/">Back</Button>
                </CardActions>
            </Card>
        </Container>
    )
}

export default NokiaDetail