import { Card, CardActions, CardContent, Typography, Button } from "@mui/material"
import { useState, useEffect } from "react";

const Samsung = ({sumTotal, setSumTotal}) => {
    const [ samsung, setSamsung ] = useState(0);
    const onBtnBuyClick = () => {
        setSamsung(samsung + 1);
    } 

    useEffect(() => {
        if(samsung > 0) 
        setSumTotal(sumTotal + 800)
    },[samsung]);

    return (
        <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    Samsung S9
                </Typography>
                <Typography>Price: 800$</Typography>
                <Typography>Quantity: {samsung}</Typography>
            </CardContent>
            <CardActions>
                <Button size="small" variant="contained" onClick={onBtnBuyClick}>Buy</Button>&nbsp;
                <Button size="small" variant="contained" href="/samsung">Detail</Button>
            </CardActions>
        </Card>
    )
}

export default Samsung