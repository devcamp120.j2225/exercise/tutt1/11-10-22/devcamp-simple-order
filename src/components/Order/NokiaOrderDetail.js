import { Card, CardContent, Typography, Button, CardActions } from "@mui/material";
import { useState, useEffect } from "react";

const Nokia = ({sumTotal, setSumTotal}) => {
    const [ nokia, setNokia ] = useState(0);
    const onBtnBuyClick = () => {
        setNokia(nokia + 1);
    }

    useEffect(() => {
        if(nokia > 0) 
        setSumTotal(sumTotal + 650)
    },[nokia]);

    return (
        <Card sx={{ minWidth: 275 }}>
            <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                    Nokia 8
                </Typography>
                <Typography>Price: 650$</Typography>
                <Typography>Quantity: {nokia}</Typography>
            </CardContent>
            <CardActions>
                <Button size="small" variant="contained" onClick={onBtnBuyClick}>Buy</Button>&nbsp;
                <Button size="small" variant="contained" href="/nokia">Detail</Button>
            </CardActions>
        </Card>
    )
}

export default Nokia